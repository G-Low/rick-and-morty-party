import React from 'react';

import { Container } from './Main.styles';

interface Props {}

const Main: React.FC<Props> = props => {
	return <Container>{props.children}</Container>;
};

export default React.memo<Props>(Main);
