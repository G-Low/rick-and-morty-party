import React from 'react';

import Card from '../../components/Card/Card';

import { PartyCard, PartyHead, PartyImages, PartyWrapper } from './Main.styles';

interface PartyProps {
	rickImageSrc?: string;
	mortyImageSrc?: string;
}

const Party: React.FC<PartyProps> = ({ rickImageSrc, mortyImageSrc }) => {
	return (
		<PartyWrapper>
			<PartyHead key="header">Party</PartyHead>
			<PartyImages key="images">
				<PartyCard key="rick">
					<Card imageSrc={rickImageSrc} label="Rick" />
				</PartyCard>
				<PartyCard key="morty">
					<Card imageSrc={mortyImageSrc} label="Morty" />
				</PartyCard>
			</PartyImages>
		</PartyWrapper>
	);
};

export default React.memo<PartyProps>(Party);
