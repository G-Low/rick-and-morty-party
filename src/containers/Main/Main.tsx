import React, { useCallback, useState } from 'react';

import { useHideCharacterMutation } from '../../system/apollo/requests';
import useCharactersFilteredLazyQuery from '../../hooks/useCharactersFilteredLazyQuery';

import SearchInput from '../../components/SearchInput/SearchInput';

import Character from './Character';
import { CharactersList } from './Main.styles';
import Party from './Party';

interface MainProps {}

const Main: React.FC<MainProps> = () => {
	const [characterName, setCharacterName] = useState<string>('');
	const [rickImageSrc, setRickImageSrc] = useState<string | undefined>();
	const [mortyImageSrc, setMortyImageSrc] = useState<string | undefined>();
	const characters = useCharactersFilteredLazyQuery(characterName);
	const [doHideCharacter] = useHideCharacterMutation();
	const onCharacterHide = useCallback<(id: number) => void>(
		(id: number) => {
			doHideCharacter({ variables: { ID: id } });
		},
		[doHideCharacter],
	);
	const onCharacterSelect = useCallback<(imageSrc: string, name?: string | null) => void>(
		(imageSrc: string, name?: string | null) => {
			if (name) {
				const nameInLowerCase = name.toLowerCase();

				if (~nameInLowerCase.indexOf('rick')) {
					setRickImageSrc(imageSrc);
				} else if (~nameInLowerCase.indexOf('morty')) {
					setMortyImageSrc(imageSrc);
				}
			}
		},
		[setRickImageSrc, setMortyImageSrc],
	);

	return (
		<div>
			<SearchInput key="search-input" onChange={setCharacterName} />
			{characters.length > 0 ? (
				<CharactersList key="characters-list">
					{characters.map(character =>
						character && character.image && character.id ? (
							<Character
								key={`character-${character.id}`}
								id={parseInt(character.id, 10)}
								imageSrc={character.image}
								name={character.name}
								onHide={onCharacterHide}
								onSelect={onCharacterSelect}
							/>
						) : null,
					)}
				</CharactersList>
			) : null}
			<Party key="party" rickImageSrc={rickImageSrc} mortyImageSrc={mortyImageSrc} />
		</div>
	);
};

export default React.memo<MainProps>(Main);
