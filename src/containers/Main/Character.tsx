import React, { useCallback } from 'react';

import Card from '../../components/Card/Card';

import { CharacterWrapper } from './Main.styles';

interface CharacterProps {
	id: number;
	name?: string | null;
	imageSrc: string;
	onHide(id: number): void;
	onSelect(imageSrc: string, name?: string | null): void;
}

const Character: React.FC<CharacterProps> = ({ id, name, onHide, onSelect, imageSrc, ...rest }) => {
	const onCardClose = useCallback<() => void>(() => onHide(id), [id, onHide]);
	const onCardClick = useCallback<() => void>(() => onSelect(imageSrc, name), [onSelect, imageSrc, name]);

	return (
		<CharacterWrapper>
			<Card {...rest} onClose={onCardClose} imageSrc={imageSrc} onClick={onCardClick} />
		</CharacterWrapper>
	);
};

export default Character;
