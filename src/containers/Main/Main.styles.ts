import styled from 'styled-components';

export const CharactersList = styled.div`
	text-align: center;
	margin: 15px -15px;
`;

export const CharacterWrapper = styled.div`
	display: inline-block;
	margin: 15px;
`;

export const PartyWrapper = styled.div`
	margin-top: 100px;
`;

export const PartyHead = styled.h2`
	text-transform: uppercase;
	font: normal 600 30px/35px Roboto, Helvetica, sans-serif;
	text-align: center;
	color: #000000;
`;

export const PartyImages = styled.div`
	text-align: center;
	margin: 0 -15px;
`;

export const PartyCard = styled.div`
	display: inline-block;
	margin: 20px 15px;
`;
