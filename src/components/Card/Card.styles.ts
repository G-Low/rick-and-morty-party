import styled from 'styled-components';

export const CardContainer = styled.div`
	position: relative;
	width: 180px;
	height: 220px;
	background-color: #dadada;
	cursor: ${props => props.onClick ? "pointer" : "default"};
`;

export const ButtonWrapper = styled.div`
	position: absolute;
	top: 8px;
	right: 8px;
`;

export const Label = styled.div`
	position: absolute;
	bottom: 27px;
	width: 100%;
	font: normal 300 24px/28px Roboto, Helvetica, sans-serif;
	text-align: center;
	text-transform: uppercase;
	color: #fff;
`;
