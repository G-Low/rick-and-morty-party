import React, { MouseEvent, useCallback } from 'react';

import Image from '../../atoms/Image/Image';
import CloseButton from '../../atoms/CloseButton/CloseButton';

import { ButtonWrapper, CardContainer, Label } from './Card.styles';

interface CardProps {
	imageSrc?: string;
	label?: string;
	onClick?(): void;
	onClose?(): void;
}

const Card: React.FC<CardProps> = ({ imageSrc, label, onClick, onClose }) => {
	const onCloseButtonClick = useCallback(
		(e: MouseEvent<HTMLButtonElement>) => {
			e.stopPropagation();

			if (onClose) {
				onClose();
			}
		},
		[onClose],
	);

	return (
		<CardContainer onClick={onClick}>
			{imageSrc ? <Image key="image" src={imageSrc} backgroundSize="cover" /> : null}
			{onClose ? (
				<ButtonWrapper key="close-button-wrapper">
					<CloseButton onClick={onCloseButtonClick} />
				</ButtonWrapper>
			) : null}
			{!imageSrc && label ? <Label key="label">{label}</Label> : null}
		</CardContainer>
	);
};

export default React.memo<CardProps>(Card);
