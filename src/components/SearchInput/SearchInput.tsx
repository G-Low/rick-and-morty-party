import React, { useEffect, useState } from 'react';

import Input from '../../atoms/Input/Input';

interface Props {
	onChange(newValue: string): void;
}

const SearchInput: React.FC<Props> = ({ onChange }) => {
	const [value, setValue] = useState<string>('');

	useEffect(() => {
		let handle: number;

		if (value.length > 2) {
			handle = setTimeout(() => onChange(value), 300);
		}

		return () => clearTimeout(handle);
	}, [value, onChange]);

	return <Input value={value} onChange={setValue} />;
};

export default React.memo<Props>(SearchInput);
