import styled from 'styled-components';

import { ImageProps } from './Image';

export const ImageContainer = styled.div<ImageProps>`
	width: 100%;
	height: 100%;
	background-image: url("${props => props.src}");
	background-size: ${props => props.backgroundSize};
`;
