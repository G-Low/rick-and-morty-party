import React from 'react';
import { BackgroundSizeProperty } from 'csstype';

import { ImageContainer } from './Image.styles';

export interface ImageProps {
	src: string;
	backgroundSize: BackgroundSizeProperty<string>;
}

const Image: React.FC<ImageProps> = props => <ImageContainer {...props} />;

export default Image;
