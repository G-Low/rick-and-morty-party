import React, { ChangeEvent, InputHTMLAttributes, useCallback } from 'react';

import { InputContainer } from './Input.styles';

interface InputProps {
	value: string;
	onChange(newValue: string): void;
}

const Input: React.FC<InputProps> = ({ value, onChange }) => {
	const onValueChange = useCallback<Required<InputHTMLAttributes<HTMLInputElement>>['onChange']>(
		(e: ChangeEvent<HTMLInputElement>) => {
			onChange(e.currentTarget.value);
		},
		[onChange],
	);

	return <InputContainer value={value} onChange={onValueChange} />;
};

export default Input;
