import styled from 'styled-components';

export const InputContainer = styled.input`
	width: 100%;
	background: #fff;
	border: 1px solid #a0a0a0;
	padding: 22px 27px 25px;
	font: normal 300 30px/35px Roboto, Helvetica, sans-serif;
`;
