import styled from 'styled-components';

export const CloseButtonContainer = styled.button`
	width: 30px;
	height: 30px;
	overflow: hidden;
	border: 0;
	border-radius: 30px;
	padding: 10px;
	background: rgba(255, 255, 255, 0.75);
`;