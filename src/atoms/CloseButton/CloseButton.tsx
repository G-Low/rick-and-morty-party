import React, { MouseEvent } from 'react';

import { CloseButtonContainer } from './CloseButton.styles';

interface CloseButtonProps {
	onClick(e: MouseEvent<HTMLButtonElement>): void;
}

const CloseButton: React.FC<CloseButtonProps> = props => {
	return (
		<CloseButtonContainer onClick={props.onClick}>
			<svg fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M10 1L9 0 5 4 1 0 0 1l4 4-4 4 1 1 4-4 4 4 1-1-4-4 4-4z" fill="#000" />
			</svg>
		</CloseButtonContainer>
	);
};

export default CloseButton;
