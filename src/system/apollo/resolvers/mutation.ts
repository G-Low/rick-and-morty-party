import { HiddenCharacterIdsDocument, MutationResolvers, HiddenCharacterIdsQuery } from '../requests';

export const Mutation: MutationResolvers = {
	hideCharacter(_, { ID }, { cache }) {
		const currentHiddenCharacterIdsData = cache.readQuery<HiddenCharacterIdsQuery>({
			query: HiddenCharacterIdsDocument,
		});
		const currentHiddenCharacterIds = currentHiddenCharacterIdsData?.hiddenCharacterIds
			? currentHiddenCharacterIdsData?.hiddenCharacterIds
			: [];

		cache.writeQuery({
			query: HiddenCharacterIdsDocument,
			data: {
				hiddenCharacterIds: currentHiddenCharacterIds.concat(ID),
			},
		});

		return true;
	},
};
