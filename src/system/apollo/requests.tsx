import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
import { ResolverCtx } from './ResolverCtx';
import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Upload: any;
};


export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}

export type Character = {
   __typename?: 'Character';
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  species?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
  origin?: Maybe<Location>;
  location?: Maybe<Location>;
  image?: Maybe<Scalars['String']>;
  episode?: Maybe<Array<Maybe<Episode>>>;
  created?: Maybe<Scalars['String']>;
};

export type Characters = {
   __typename?: 'Characters';
  info?: Maybe<Info>;
  results?: Maybe<Array<Maybe<Character>>>;
};

export type Episode = {
   __typename?: 'Episode';
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  air_date?: Maybe<Scalars['String']>;
  episode?: Maybe<Scalars['String']>;
  characters?: Maybe<Array<Maybe<Character>>>;
  created?: Maybe<Scalars['String']>;
};

export type Episodes = {
   __typename?: 'Episodes';
  info?: Maybe<Info>;
  results?: Maybe<Array<Maybe<Episode>>>;
};

export type FilterCharacter = {
  name?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  species?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
};

export type FilterEpisode = {
  name?: Maybe<Scalars['String']>;
  episode?: Maybe<Scalars['String']>;
};

export type FilterLocation = {
  name?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  dimension?: Maybe<Scalars['String']>;
};

export type Info = {
   __typename?: 'Info';
  count?: Maybe<Scalars['Int']>;
  pages?: Maybe<Scalars['Int']>;
  next?: Maybe<Scalars['Int']>;
  prev?: Maybe<Scalars['Int']>;
};

export type Location = {
   __typename?: 'Location';
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  dimension?: Maybe<Scalars['String']>;
  residents?: Maybe<Array<Maybe<Character>>>;
  created?: Maybe<Scalars['String']>;
};

export type Locations = {
   __typename?: 'Locations';
  info?: Maybe<Info>;
  results?: Maybe<Array<Maybe<Location>>>;
};

export type Mutation = {
   __typename?: 'Mutation';
  /** Записывает ID персонажа в список скрытых */
  hideCharacter: Scalars['Boolean'];
};


export type MutationHideCharacterArgs = {
  ID: Scalars['Int'];
};

export type Query = {
   __typename?: 'Query';
  character?: Maybe<Character>;
  characters?: Maybe<Characters>;
  episode?: Maybe<Episode>;
  episodes?: Maybe<Episodes>;
  /** Список ID скрытых персонажей */
  hiddenCharacterIds: Array<Scalars['Int']>;
  location?: Maybe<Location>;
  locations?: Maybe<Locations>;
};


export type QueryCharacterArgs = {
  id?: Maybe<Scalars['ID']>;
};


export type QueryCharactersArgs = {
  page?: Maybe<Scalars['Int']>;
  filter?: Maybe<FilterCharacter>;
};


export type QueryEpisodeArgs = {
  id?: Maybe<Scalars['ID']>;
};


export type QueryEpisodesArgs = {
  page?: Maybe<Scalars['Int']>;
  filter?: Maybe<FilterEpisode>;
};


export type QueryLocationArgs = {
  id?: Maybe<Scalars['ID']>;
};


export type QueryLocationsArgs = {
  page?: Maybe<Scalars['Int']>;
  filter?: Maybe<FilterLocation>;
};


export type HideCharacterMutationVariables = {
  ID: Scalars['Int'];
};


export type HideCharacterMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'hideCharacter'>
);

export type CharactersQueryVariables = {
  name: Scalars['String'];
};


export type CharactersQuery = (
  { __typename?: 'Query' }
  & { characters?: Maybe<(
    { __typename?: 'Characters' }
    & { results?: Maybe<Array<Maybe<(
      { __typename?: 'Character' }
      & Pick<Character, 'id' | 'name' | 'image'>
    )>>> }
  )> }
);

export type HiddenCharacterIdsQueryVariables = {};


export type HiddenCharacterIdsQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'hiddenCharacterIds'>
);



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type StitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type isTypeOfResolverFn<T = {}> = (obj: T, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Query: ResolverTypeWrapper<{}>,
  ID: ResolverTypeWrapper<Scalars['ID']>,
  Character: ResolverTypeWrapper<Character>,
  String: ResolverTypeWrapper<Scalars['String']>,
  Location: ResolverTypeWrapper<Location>,
  Episode: ResolverTypeWrapper<Episode>,
  Int: ResolverTypeWrapper<Scalars['Int']>,
  FilterCharacter: FilterCharacter,
  Characters: ResolverTypeWrapper<Characters>,
  Info: ResolverTypeWrapper<Info>,
  FilterEpisode: FilterEpisode,
  Episodes: ResolverTypeWrapper<Episodes>,
  FilterLocation: FilterLocation,
  Locations: ResolverTypeWrapper<Locations>,
  Mutation: ResolverTypeWrapper<{}>,
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>,
  CacheControlScope: CacheControlScope,
  Upload: ResolverTypeWrapper<Scalars['Upload']>,
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Query: {},
  ID: Scalars['ID'],
  Character: Character,
  String: Scalars['String'],
  Location: Location,
  Episode: Episode,
  Int: Scalars['Int'],
  FilterCharacter: FilterCharacter,
  Characters: Characters,
  Info: Info,
  FilterEpisode: FilterEpisode,
  Episodes: Episodes,
  FilterLocation: FilterLocation,
  Locations: Locations,
  Mutation: {},
  Boolean: Scalars['Boolean'],
  CacheControlScope: CacheControlScope,
  Upload: Scalars['Upload'],
};

export type ClientDirectiveArgs = {   always?: Maybe<Scalars['Boolean']>; };

export type ClientDirectiveResolver<Result, Parent, ContextType = ResolverCtx, Args = ClientDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type CharacterResolvers<ContextType = ResolverCtx, ParentType extends ResolversParentTypes['Character'] = ResolversParentTypes['Character']> = {
  id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  status?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  species?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  gender?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  origin?: Resolver<Maybe<ResolversTypes['Location']>, ParentType, ContextType>,
  location?: Resolver<Maybe<ResolversTypes['Location']>, ParentType, ContextType>,
  image?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  episode?: Resolver<Maybe<Array<Maybe<ResolversTypes['Episode']>>>, ParentType, ContextType>,
  created?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type CharactersResolvers<ContextType = ResolverCtx, ParentType extends ResolversParentTypes['Characters'] = ResolversParentTypes['Characters']> = {
  info?: Resolver<Maybe<ResolversTypes['Info']>, ParentType, ContextType>,
  results?: Resolver<Maybe<Array<Maybe<ResolversTypes['Character']>>>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type EpisodeResolvers<ContextType = ResolverCtx, ParentType extends ResolversParentTypes['Episode'] = ResolversParentTypes['Episode']> = {
  id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  air_date?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  episode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  characters?: Resolver<Maybe<Array<Maybe<ResolversTypes['Character']>>>, ParentType, ContextType>,
  created?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type EpisodesResolvers<ContextType = ResolverCtx, ParentType extends ResolversParentTypes['Episodes'] = ResolversParentTypes['Episodes']> = {
  info?: Resolver<Maybe<ResolversTypes['Info']>, ParentType, ContextType>,
  results?: Resolver<Maybe<Array<Maybe<ResolversTypes['Episode']>>>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type InfoResolvers<ContextType = ResolverCtx, ParentType extends ResolversParentTypes['Info'] = ResolversParentTypes['Info']> = {
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  pages?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  next?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  prev?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type LocationResolvers<ContextType = ResolverCtx, ParentType extends ResolversParentTypes['Location'] = ResolversParentTypes['Location']> = {
  id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  dimension?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  residents?: Resolver<Maybe<Array<Maybe<ResolversTypes['Character']>>>, ParentType, ContextType>,
  created?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type LocationsResolvers<ContextType = ResolverCtx, ParentType extends ResolversParentTypes['Locations'] = ResolversParentTypes['Locations']> = {
  info?: Resolver<Maybe<ResolversTypes['Info']>, ParentType, ContextType>,
  results?: Resolver<Maybe<Array<Maybe<ResolversTypes['Location']>>>, ParentType, ContextType>,
  __isTypeOf?: isTypeOfResolverFn<ParentType>,
};

export type MutationResolvers<ContextType = ResolverCtx, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
  hideCharacter?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType, RequireFields<MutationHideCharacterArgs, 'ID'>>,
};

export type QueryResolvers<ContextType = ResolverCtx, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  character?: Resolver<Maybe<ResolversTypes['Character']>, ParentType, ContextType, RequireFields<QueryCharacterArgs, never>>,
  characters?: Resolver<Maybe<ResolversTypes['Characters']>, ParentType, ContextType, RequireFields<QueryCharactersArgs, never>>,
  episode?: Resolver<Maybe<ResolversTypes['Episode']>, ParentType, ContextType, RequireFields<QueryEpisodeArgs, never>>,
  episodes?: Resolver<Maybe<ResolversTypes['Episodes']>, ParentType, ContextType, RequireFields<QueryEpisodesArgs, never>>,
  hiddenCharacterIds?: Resolver<Array<ResolversTypes['Int']>, ParentType, ContextType>,
  location?: Resolver<Maybe<ResolversTypes['Location']>, ParentType, ContextType, RequireFields<QueryLocationArgs, never>>,
  locations?: Resolver<Maybe<ResolversTypes['Locations']>, ParentType, ContextType, RequireFields<QueryLocationsArgs, never>>,
};

export interface UploadScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Upload'], any> {
  name: 'Upload'
}

export type Resolvers<ContextType = ResolverCtx> = {
  Character?: CharacterResolvers<ContextType>,
  Characters?: CharactersResolvers<ContextType>,
  Episode?: EpisodeResolvers<ContextType>,
  Episodes?: EpisodesResolvers<ContextType>,
  Info?: InfoResolvers<ContextType>,
  Location?: LocationResolvers<ContextType>,
  Locations?: LocationsResolvers<ContextType>,
  Mutation?: MutationResolvers<ContextType>,
  Query?: QueryResolvers<ContextType>,
  Upload?: GraphQLScalarType,
};


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
*/
export type IResolvers<ContextType = ResolverCtx> = Resolvers<ContextType>;
export type DirectiveResolvers<ContextType = ResolverCtx> = {
  client?: ClientDirectiveResolver<any, any, ContextType>,
};


/**
* @deprecated
* Use "DirectiveResolvers" root object instead. If you wish to get "IDirectiveResolvers", add "typesPrefix: I" to your config.
*/
export type IDirectiveResolvers<ContextType = ResolverCtx> = DirectiveResolvers<ContextType>;

export const HideCharacterDocument = gql`
    mutation HideCharacter($ID: Int!) {
  hideCharacter(ID: $ID) @client
}
    `;
export type HideCharacterMutationFn = ApolloReactCommon.MutationFunction<HideCharacterMutation, HideCharacterMutationVariables>;
export type HideCharacterComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<HideCharacterMutation, HideCharacterMutationVariables>, 'mutation'>;

    export const HideCharacterComponent = (props: HideCharacterComponentProps) => (
      <ApolloReactComponents.Mutation<HideCharacterMutation, HideCharacterMutationVariables> mutation={HideCharacterDocument} {...props} />
    );
    

/**
 * __useHideCharacterMutation__
 *
 * To run a mutation, you first call `useHideCharacterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useHideCharacterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [hideCharacterMutation, { data, loading, error }] = useHideCharacterMutation({
 *   variables: {
 *      ID: // value for 'ID'
 *   },
 * });
 */
export function useHideCharacterMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<HideCharacterMutation, HideCharacterMutationVariables>) {
        return ApolloReactHooks.useMutation<HideCharacterMutation, HideCharacterMutationVariables>(HideCharacterDocument, baseOptions);
      }
export type HideCharacterMutationHookResult = ReturnType<typeof useHideCharacterMutation>;
export type HideCharacterMutationResult = ApolloReactCommon.MutationResult<HideCharacterMutation>;
export type HideCharacterMutationOptions = ApolloReactCommon.BaseMutationOptions<HideCharacterMutation, HideCharacterMutationVariables>;
export const CharactersDocument = gql`
    query Characters($name: String!) {
  characters(page: 0, filter: {name: $name}) {
    results {
      id
      name
      image
    }
  }
}
    `;
export type CharactersComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<CharactersQuery, CharactersQueryVariables>, 'query'> & ({ variables: CharactersQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const CharactersComponent = (props: CharactersComponentProps) => (
      <ApolloReactComponents.Query<CharactersQuery, CharactersQueryVariables> query={CharactersDocument} {...props} />
    );
    

/**
 * __useCharactersQuery__
 *
 * To run a query within a React component, call `useCharactersQuery` and pass it any options that fit your needs.
 * When your component renders, `useCharactersQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCharactersQuery({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useCharactersQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<CharactersQuery, CharactersQueryVariables>) {
        return ApolloReactHooks.useQuery<CharactersQuery, CharactersQueryVariables>(CharactersDocument, baseOptions);
      }
export function useCharactersLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<CharactersQuery, CharactersQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<CharactersQuery, CharactersQueryVariables>(CharactersDocument, baseOptions);
        }
export type CharactersQueryHookResult = ReturnType<typeof useCharactersQuery>;
export type CharactersLazyQueryHookResult = ReturnType<typeof useCharactersLazyQuery>;
export type CharactersQueryResult = ApolloReactCommon.QueryResult<CharactersQuery, CharactersQueryVariables>;
export const HiddenCharacterIdsDocument = gql`
    query HiddenCharacterIds {
  hiddenCharacterIds @client
}
    `;
export type HiddenCharacterIdsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<HiddenCharacterIdsQuery, HiddenCharacterIdsQueryVariables>, 'query'>;

    export const HiddenCharacterIdsComponent = (props: HiddenCharacterIdsComponentProps) => (
      <ApolloReactComponents.Query<HiddenCharacterIdsQuery, HiddenCharacterIdsQueryVariables> query={HiddenCharacterIdsDocument} {...props} />
    );
    

/**
 * __useHiddenCharacterIdsQuery__
 *
 * To run a query within a React component, call `useHiddenCharacterIdsQuery` and pass it any options that fit your needs.
 * When your component renders, `useHiddenCharacterIdsQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHiddenCharacterIdsQuery({
 *   variables: {
 *   },
 * });
 */
export function useHiddenCharacterIdsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<HiddenCharacterIdsQuery, HiddenCharacterIdsQueryVariables>) {
        return ApolloReactHooks.useQuery<HiddenCharacterIdsQuery, HiddenCharacterIdsQueryVariables>(HiddenCharacterIdsDocument, baseOptions);
      }
export function useHiddenCharacterIdsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<HiddenCharacterIdsQuery, HiddenCharacterIdsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<HiddenCharacterIdsQuery, HiddenCharacterIdsQueryVariables>(HiddenCharacterIdsDocument, baseOptions);
        }
export type HiddenCharacterIdsQueryHookResult = ReturnType<typeof useHiddenCharacterIdsQuery>;
export type HiddenCharacterIdsLazyQueryHookResult = ReturnType<typeof useHiddenCharacterIdsLazyQuery>;
export type HiddenCharacterIdsQueryResult = ApolloReactCommon.QueryResult<HiddenCharacterIdsQuery, HiddenCharacterIdsQueryVariables>;