import { HttpLink } from 'apollo-link-http';
import ApolloClient from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import typeDefs from '../apollo/local.graphql';
import * as resolvers from '../apollo/resolvers';

const cache = new InMemoryCache();

async function initCache(): Promise<void> {
  cache.writeData({
    data: {
      hiddenCharacterIds: [],
    },
  });
}

initCache().catch(console.log);

export const client = new ApolloClient({
  link: new HttpLink({ uri: 'https://rickandmortyapi.com/graphql' }),
  cache,
  typeDefs,
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  resolvers,
});

client.onResetStore(initCache);
client.onClearStore(initCache);
