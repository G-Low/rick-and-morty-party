import React from 'react';
import { ApolloProvider } from '@apollo/react-hooks';

import { client } from './system/apollo/apollo';

import MainLayout from './layouts/Main/Main';
import MainContainer from './containers/Main/Main';

function App() {
	return (
		<ApolloProvider client={client}>
			<MainLayout>
				<MainContainer />
			</MainLayout>
		</ApolloProvider>
	);
}

export default App;
