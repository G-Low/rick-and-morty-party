import { useEffect, useState } from 'react';

import {
	Character,
	useCharactersLazyQuery,
	useHiddenCharacterIdsQuery,
} from '../system/apollo/requests';

type Characters = Array<{ __typename?: 'Character' } & Pick<Character, 'id' | 'name' | 'image'>>;

function useCharactersFilteredLazyQuery(
	characterName: string,
): Characters {
	const [filteredCharacters, setFilteredCharacters] = useState<Characters>([]);
	const { data: hiddenCharacterIdsData } = useHiddenCharacterIdsQuery();
	const [getCharacters, { data, called, refetch }] = useCharactersLazyQuery({ variables: { name: characterName } });
	const hiddenCharacterIds = hiddenCharacterIdsData?.hiddenCharacterIds;

	useEffect(() => {
		if (characterName.length > 2) {
			if (called && refetch) {
				refetch({ name: characterName });

				return;
			}

			getCharacters();
		}
	}, [characterName, getCharacters, refetch, called]);

	useEffect(() => {
		if (data && data.characters && Array.isArray(data.characters.results)) {
			let newFilteredCharacters: Characters = Array(6);

			if (Array.isArray(hiddenCharacterIds)) {
				const characters = data.characters.results;
				const charactersCount = characters.length;
				let j = 0;

				for (let i = 0; i < charactersCount && j < 6; i++) {
					const character = characters[i];

					if (character?.id && !hiddenCharacterIds.includes(parseInt(character.id, 10))) {
						newFilteredCharacters[j] = character;
						j++;
					}
				}

				setFilteredCharacters(newFilteredCharacters.slice(0, j));
			}
		} else if (!data?.characters?.results) {
			setFilteredCharacters([]);
		}
	}, [data, hiddenCharacterIds]);

	return filteredCharacters;
}

export default useCharactersFilteredLazyQuery;
