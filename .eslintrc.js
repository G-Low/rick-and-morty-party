module.exports = {
    parser: '@typescript-eslint/parser',
    extends: [
        'plugin:react/recommended',
        'plugin:@typescript-eslint/recommended',
        'prettier/@typescript-eslint',
        'plugin:prettier/recommended',
    ],
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
    },
    settings: {
        react: {
            version: 'detect',
        },
    },
    rules: {
        'no-explicit-any': 0,
        '@typescript-eslint/no-explicit-any': 0,
        '@typescript-eslint/camelcase': 0,
    },
    ignorePatterns: ['src/system/apollo/requests.tsx'],
};